package main

import (
	"encoding/json"
	"os"
)

type Config struct {
	TelegramAPIToken string `json:"telegram_api_token"`
	WebhookURL       string `json:"webhook_url"`
	GiphyAPIKey      string `json:"giphy_api_key"`
}

func NewConfigFromFile(filepath string) (*Config, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	cfg := &Config{}
	if err := json.NewDecoder(file).Decode(&cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}
