package giphy

type Random struct {
	Data struct {
		Type             string           `json:"type"`
		ID               string           `json:"id"`
		Slug             string           `json:"slug"`
		URL              string           `json:"url"`
		BitlyGifURL      string           `json:"bitly_gif_url"`
		BitlyURL         string           `json:"bitly_url"`
		EmbedURL         string           `json:"embed_url"`
		Username         string           `json:"username"`
		Source           string           `json:"source"`
		ContentURL       string           `json:"content_url"`
		SourceTld        string           `json:"source_tld"`
		SourcePostURL    string           `json:"source_post_url"`
		IsIndexable      int              `json:"is_indexable"`
		IsSticker        int              `json:"is_sticker"`
		ImportDatetime   string           `json:"import_datetime"`
		TrendingDatetime string           `json:"trending_datetime"`
		Images           map[string]image `json:"images"`
	} `json:"data"`
	Meta meta `json:"meta"`
}

type image struct {
	URL      string `json:"url"`
	Width    string `json:"width"`
	Height   string `json:"height"`
	Size     string `json:"size"`
	Frames   string `json:"frames"`
	MP4      string `json:"mp4"`
	MP4Size  string `json:"mp4_size"`
	Webp     string `json:"webp"`
	WebpSize string `json:"webp_size"`
}

type meta struct {
	Status     int    `json:"status"`
	Msg        string `json:"msg"`
	ResponseID string `json:"response_id"`
}
