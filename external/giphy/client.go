package giphy

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	API_ROOT = "https://api.giphy.com/v1"
)

type GiphyAPI struct {
	ApiKey string
	Rating string `default:"R"`
	Client *http.Client
}

func NewGiphyAPI(apiKey string) *GiphyAPI {
	return &GiphyAPI{ApiKey: apiKey, Client: &http.Client{}}
}

func (g *GiphyAPI) GetRandom(tag string) (*Random, error) {
	url := fmt.Sprintf("%v/gifs/random?api_key=%v&tag=%v&rating=%v", API_ROOT, g.ApiKey, tag, g.Rating)
	resp, err := g.Client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	random := new(Random)
	if err := json.NewDecoder(resp.Body).Decode(random); err != nil {
		return nil, err
	}

	return random, nil
}
