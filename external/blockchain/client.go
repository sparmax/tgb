package blockchain

import (
	"encoding/json"
	"net/http"
)

const (
	API_ROOT        = "https://blockchain.info"
	TICKER_ENDPOINT = "/ticker"
)

type BlockchainAPI struct {
	Client *http.Client
}

type TickerResponse struct {
	USD struct {
		Last float64 `json:"last"`
	} `json:"USD"`
}

func (b *BlockchainAPI) GetTickerResponse() (*TickerResponse, error) {
	resp, err := b.Client.Get(API_ROOT + TICKER_ENDPOINT)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	ticker := new(TickerResponse)
	if err := json.NewDecoder(resp.Body).Decode(ticker); err != nil {
		return nil, err
	}

	return ticker, nil
}

func NewBlockchainAPI() *BlockchainAPI {
	return &BlockchainAPI{Client: &http.Client{}}
}
