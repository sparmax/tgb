package main

import (
	"errors"
	"fmt"
	"tgbot/external/blockchain"
	"tgbot/external/giphy"

	"gopkg.in/telegram-bot-api.v4"
)

const (
	ErrorMessage = "Something is wrong with service or bot. Please try again later."
)

type CommandRegister struct {
	commands       map[string]BotCommand
	defaultCommand BotCommand
}

func NewCommandRegister(defaultCommand BotCommand) *CommandRegister {
	register := &CommandRegister{defaultCommand: defaultCommand}
	register.commands = make(map[string]BotCommand)
	return register
}

func (c *CommandRegister) AddCommand(key string, command BotCommand) {
	c.commands[key] = command
}

func (c *CommandRegister) GetCommand(key string) BotCommand {
	command, ok := c.commands[key]
	if !ok {
		return c.defaultCommand
	}
	return command
}

type BotCommand interface {
	MakeResponseMessage(chatID int64) (tgbotapi.MessageConfig, error)
}

type Ticker struct {
	blockchainAPI *blockchain.BlockchainAPI
}

type Cage struct {
	giphyAPI *giphy.GiphyAPI
}

type Help struct {
}

type Default struct {
}

func NewTickerCommand(blockchainAPI *blockchain.BlockchainAPI) *Ticker {
	return &Ticker{blockchainAPI: blockchainAPI}
}

func (command *Ticker) MakeResponseMessage(chatID int64) (tgbotapi.MessageConfig, error) {
	resp, err := command.blockchainAPI.GetTickerResponse()
	if err != nil {
		return tgbotapi.NewMessage(chatID, ErrorMessage), err
	}

	return tgbotapi.NewMessage(chatID, fmt.Sprintf("BTC/USD: %v", int(resp.USD.Last))), nil
}

func NewCageCommand(giphyAPI *giphy.GiphyAPI) *Cage {
	return &Cage{giphyAPI: giphyAPI}
}

func (command *Cage) MakeResponseMessage(chatID int64) (tgbotapi.MessageConfig, error) {
	random, err := command.giphyAPI.GetRandom("nicolas-cage")
	if err != nil {
		return tgbotapi.NewMessage(chatID, ErrorMessage), err
	}

	image, ok := random.Data.Images["original"]
	if !ok {
		return tgbotapi.NewMessage(chatID, ErrorMessage), errors.New("Bad response from Giphy API")
	}

	return tgbotapi.NewMessage(chatID, image.MP4), nil
}

func NewHelpCommand() *Help {
	return &Help{}
}

func (command *Help) MakeResponseMessage(chatID int64) (tgbotapi.MessageConfig, error) {
	msg := "Supported commands:\n/ticker - current BTC/USD exchange rate\n/cage - random Nicolas Cage gif\n/help - show command list"
	return tgbotapi.NewMessage(chatID, msg), nil
}

func NewDefaultCommand() *Default {
	return &Default{}
}

func (command *Default) MakeResponseMessage(chatID int64) (tgbotapi.MessageConfig, error) {
	msg := "Unknown command. Use /help to show a list of commands"
	return tgbotapi.NewMessage(chatID, msg), nil
}
