package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gopkg.in/telegram-bot-api.v4"

	"tgbot/external/blockchain"
	"tgbot/external/giphy"
)

type TelegramBot struct {
	botAPI          *tgbotapi.BotAPI
	commandRegister *CommandRegister
}

var (
	loggerInfo  = log.New(os.Stdout, "INFO: ", log.LstdFlags)
	loggerError = log.New(os.Stderr, "ERR: ", log.LstdFlags)
)

func main() {
	currentDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatalln(err)
	}

	addr := flag.String("addr", "0.0.0.0:443", "listening address with port")
	cfgpath := flag.String("config", filepath.Join(currentDir, "conf.json"), "json config filepath")
	certpath := flag.String("cert", filepath.Join(currentDir, "cert.pem"), "cert.pem filepath")
	keypath := flag.String("key", filepath.Join(currentDir, "key.pem"), "key.pem filepath")

	flag.Parse()

	cfg, err := NewConfigFromFile(*cfgpath)
	if err != nil {
		log.Fatalln(err)
	}

	tgbot, err := NewTelegramBot(cfg, *certpath)
	if err != nil {
		log.Fatalln(err)
	}

	go func() {
		log.Printf("Start listening %s\n", *addr)
		if err := http.ListenAndServeTLS(*addr, *certpath, *keypath, nil); err != nil {
			log.Fatalln(err)
		}

	}()

	tgbot.run()
}

func NewTelegramBot(cfg *Config, cert string) (*TelegramBot, error) {
	bot, err := tgbotapi.NewBotAPI(cfg.TelegramAPIToken)
	if err != nil {
		return nil, err
	}

	_, err = bot.SetWebhook(tgbotapi.NewWebhookWithCert(cfg.WebhookURL+bot.Token, cert))
	if err != nil {
		return nil, err
	}

	register := NewCommandRegister(NewDefaultCommand())
	register.AddCommand("/ticker", NewTickerCommand(blockchain.NewBlockchainAPI()))
	register.AddCommand("/cage", NewCageCommand(giphy.NewGiphyAPI(cfg.GiphyAPIKey)))
	register.AddCommand("/help", NewHelpCommand())

	return &TelegramBot{bot, register}, nil
}

func (tgbot *TelegramBot) run() {
	updatesChan := tgbot.botAPI.ListenForWebhook("/")

	for u := range updatesChan {
		if u.Message == nil {
			continue
		}

		command := tgbot.commandRegister.GetCommand(u.Message.Text)

		msg, err := command.MakeResponseMessage(u.Message.Chat.ID)
		if err != nil {
			loggerError.Printf("external, user: %s, cmd: '%s', err: %s", u.Message.Chat.UserName, u.Message.Text, err.Error())
		}

		if _, err := tgbot.botAPI.Send(msg); err != nil {
			loggerError.Printf("telegram, user: %s, cmd: '%s', err: %s", u.Message.Chat.UserName, u.Message.Text, err.Error())
		} else {
			loggerInfo.Printf("user: %s, cmd: '%s', resp: '%s'", u.Message.Chat.UserName, u.Message.Text, msg.Text)
		}
	}
}
